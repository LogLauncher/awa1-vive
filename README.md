# AWA1 - Project

Author: Struan Forsyth<br>
Class: SI-T2a<br>
Date: 15.01.2018

## Description

The project consists of creating a game in Unity for the HTC Vive.<br>
The goal of the game is to collect the fruits and vegetables that spawn in the play area and place them into there respective barrels.

## Game mechanics

The player can move around the map by teleporting freely to the item to pick up or just to explore the space.<br>
The player can pick up the food that spawns in the map. Once the food is in the player's hand they need to return to the barrels and place/throw the food into the correct one.<br>
The food has an expiration time. From the moment it appears it will start to get smaller and eventually disappear altogether. The game has a time limit that is displayed in the sky alongside the score.<br>
The score depends on the size of the food and its type when placed into a barrel. If the incorrect food is placed in a barrel the score will be reduced.

## Inspiration

The original idea came from a classmate named "Antoine Dessauges".<br>
The idea was that the player would be stationary and the food would spawn in his reach. You would then grab the food and throw it into the correct barrel.

As I personally was interested in a bigger world that the player could explore by teleporting, I decided that the food would spawn in a wide area that you would need to go and grab before returning to the barrels.

## Deployment

To continue the project, you can simply clone the Git-Lab repository or download and extract the zip to the desired location.

- To clone the repository simply navigate to the desired location and execute the following command: `git clone https://gitlab.com/LogLauncher/awa1-vive.git`
- To download the zip simply got to the following link to download the zip and extract it to the desired location: <https://gitlab.com/LogLauncher/awa1-vive.git>

With unity navigate to and open the project folder. You can now start modifying the game!!

In the `Build` folder you will find the .exe file to launching the game without using Unity.

<br><br><br>

## Technologies

The project was created using the following technologies:

- Unity
- SteamVR
- VRTK
- C#

3D models used where taken from the Unity asset store and are free to use:

- LowPoly Environment Pack
- Lowpoly Vegetation Pack Free

## Game screenshots

| ![Barrels](Screenshots/Barrels.png)         | ![Score and Time](Screenshots/Score_and_Time.png)
| :------------------------------------------ | :------------------------------------------------
| ![Nice corner](Screenshots/Nice_corner.png) | ![Relax zone](Screenshots/Relax_zone.png)
| ![Hidden zone](Screenshots/Hidden_zone.png) | ![Banana toss](Screenshots/Throw_Banana.png)
