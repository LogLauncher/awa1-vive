﻿using UnityEngine;
using VRTK;

/// <inheritdoc />
/// <summary>
/// Code for the restart game
/// </summary>
public class RestartGame : MonoBehaviour {

    public VRTK_ControllerEvents ControllerEvents;  // Script that manages all the controller events/actions

    /// <summary>
    /// Unity function
    /// Called when the object is enabled
    /// Adds the function to the controller event
    /// </summary>
    private void OnEnable() {
        //Adds the function to the controller event
        ControllerEvents.ButtonTwoReleased += ControllerEventsOnButtonTwoReleased;
    }

    /// <summary>
    /// Unity funtion
    /// Called when the object is disabled
    /// </summary>
    private void OnDisable() {
        //Adds the function to the controller event
        ControllerEvents.ButtonTwoReleased -= ControllerEventsOnButtonTwoReleased;
    }

    /// <summary>
    /// Function for when the Menu/Restart button is pressed
    /// </summary>
    /// <param name="sender">Which object grabbed this one</param>
    /// <param name="controllerInteractionEventArgs">Arguments from the interactable object event</param>
    private void ControllerEventsOnButtonTwoReleased(object sender, ControllerInteractionEventArgs controllerInteractionEventArgs) {
        // Call the restart function in GameManager
        GameManager.Instance.Restart();
    }
}
