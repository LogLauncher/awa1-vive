﻿using UnityEngine;
using UnityEngine.UI;

/// <inheritdoc />
/// <summary>
/// Code for a barrel object
/// </summary>
public class Barrel : MonoBehaviour {
    public Food.Type FoodType;      // Type of food the barrel accepts
    public Transform Sign;          // Position of the food to show what type the barrel accepts
    public Text ItemCountText;      // Text to display how many of that food was collected in the barrel

    private int _foodCount;         // Number of food items the player put into the barrel

    /// <summary>
    /// Unity function
    /// Called once when the object is instantiated in the world
    /// </summary>
    private void Start() {
        // Load the food from a prefab
        var foodLoad = Resources.Load(FoodType.ToString()) as GameObject;
        // Check if null, stop execution if null
        if (foodLoad == null) return;
        // Remove the food tag so it doesn't count as a collected item
        foodLoad.tag = "Untagged";
        // Added the loaded food into the world
        var food = Instantiate(foodLoad, Sign.position, Quaternion.identity, Sign);
        // Set it's rotation to 90°
        food.transform.localRotation = Quaternion.Euler(0f, 90f, 0f);
    }

    /// <summary>
    /// Unity function
    /// Called when an object enters the collider
    /// </summary>
    /// <param name="other">The object that entered the collider</param>
    private void OnTriggerEnter(Collider other) {
        // Check if it's food, if not stop execution
        if (!other.CompareTag("Food")) return;
        // Get the food script from the object
        var food = other.GetComponent<Food>();
        // Check if the food type is the same
        if (food.FoodType == FoodType) {
            // The correct food was put into the barrel
            Correct(food);
        } else {
            // The incorrect food was put into the barrel
            Wrong(food);
        }
    }

    /// <summary>
    /// Function for if the incorrect food was put into the barrel
    /// </summary>
    /// <param name="food">The food that was put into the barrel</param>
    private void Wrong(Food food) {
        // Check that the food was already sorted or that we aren't playing the game, stop the execution
        if (food.Sorted || GameManager.Instance.GameStatus != GameManager.Status.Playing) return;
        // Remove 25 points from the score
        GameManager.Instance.Score -= 25;
        // Destroy the food object
        Destroy(food.gameObject);
    }

    /// <summary>
    /// Function for if the correct food was put into the barrel
    /// </summary>
    /// <param name="food">The food that was put into the barrel</param>
    private void Correct(Food food) {
        // Check that the food was already sorted or that we aren't playing the game, stop the execution
        if (food.Sorted || GameManager.Instance.GameStatus != GameManager.Status.Playing) return;
        // Stops the foods decaying process
        food.StopDecay();
        // Add the food points to the score
        GameManager.Instance.Score += (int)food.Score;
        // The food has been sorted
        food.Sorted = true;
        // Add 1 to the food counter for this barrel
        _foodCount++;
        // Display the new food counter
        ItemCountText.text = _foodCount.ToString();
    }
}