﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

/// <inheritdoc />
/// <summary>
/// Code that will spawn food
/// </summary>
public class SpawnFood : MonoBehaviour {
    public Transform FoodParent;        // The object in which the food will be put (keep unity Hierarchy clean) 

    private float _nextTime;            // The time before the next food spawn
    private float _spawnRate = 7.5f;    // The time between spawns

    private void Update() {
        //Check that we can spawn the next food
        if (!(_nextTime < Time.timeSinceLevelLoad) || GameManager.Instance.GameStatus != GameManager.Status.Playing) return;
        // Call to the function to spawn the food
        Spawn();
        //Set next time to spawn a food
        _nextTime = Time.timeSinceLevelLoad + _spawnRate;

        //Speed up the spawnrate for the next food
        _spawnRate *= 0.95f;
        // Keeps the value between 1 and 100
        _spawnRate = Mathf.Clamp(_spawnRate, 1f, 100f);
    }

    private void Spawn() {
        // Choose a random food to spawn
        var nbFoodTypes = Enum.GetNames(typeof(Food.Type)).Length;
        var foodType = Enum.GetNames(typeof(Food.Type))[Random.Range(1, nbFoodTypes)];

        // Set a random position to spawn the food
        var randPos = new Vector3(
            Random.Range(transform.position.x - transform.localScale.x / 2,
                transform.position.x + transform.localScale.x / 2),
            Random.Range(transform.position.y - transform.localScale.y / 2,
                transform.position.y + transform.localScale.y / 2),
            Random.Range(transform.position.z - transform.localScale.z / 2,
                transform.position.z + transform.localScale.z / 2)
        );

        // Spawn the food
        var food = Instantiate(Resources.Load(foodType), randPos, Quaternion.identity, FoodParent) as GameObject;
        // Check if it was spawned
        if (food == null) return;
        // Add physics to the food
        food.AddComponent<Rigidbody>();
        // Set tag. Used to check item to pickup and for the score
        food.tag = "Food";
        // Initialize the food (decay time, score, etc.)
        var foodScript = food.GetComponent<Food>();
        foodScript.Init();
    }
}