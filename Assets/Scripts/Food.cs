﻿using UnityEngine;
using VRTK;

/// <inheritdoc />
/// <summary>
/// Code for a food object
/// </summary>
public class Food : MonoBehaviour {
    // The types of food in the game
    public enum Type {
        None,
        Apple,
        Banana,
        Carrot,
        Corn,
        Mushroom
    }


    private Vector3 _scaleReduction;    // The amount to reduce the scale by on every decay tick
    private float _scoreReduction;      // The amount to reduce the food points by on every decay tick

    private int _timer;                 // The timer for the decay (will change)
    public int DecayTime;               // The initial time for the decay (will not change)

    public Type FoodType;               // The type of food this object is
    public int ScoreValue;              // The initial amount of point this food object has

    public bool Sorted;                 // If the food item has already been sorted

    // The number of points the food item is worth
    public float Score { get; private set; }

    /// <summary>
    /// Function to initialize a food object when spawned
    /// </summary>
    public void Init() {
        // The food hasn't been sorted
        Sorted = false;
        // Set the timer
        _timer = DecayTime;
        // Set the number of points
        Score = ScoreValue;
        // Set the value to reduce the points by every decay tick
        _scoreReduction = ScoreValue / (float)DecayTime;
        // Set the value to reduce the scale by every decay tick
        _scaleReduction = transform.localScale / DecayTime;
        // Start the decay process
        StartDecay();

        // Get the interactable object script
        var interactableObject = GetComponent<VRTK_InteractableObject>();
        // Add the grabbed function to the event in the interactable object script
        interactableObject.InteractableObjectGrabbed += OnInteractableObjectGrabbed;
        // Add the released function to the event in the interactable object script
        interactableObject.InteractableObjectUngrabbed += OnInteractableObjectUngrabbed;

    }

    /// <summary>
    /// Function with the logic of what to do when the object is grabbed
    /// </summary>
    /// <param name="sender">Which object grabbed this one</param>
    /// <param name="interactableObjectEventArgs">Arguments from the interactable object event</param>
    private void OnInteractableObjectGrabbed(object sender, InteractableObjectEventArgs interactableObjectEventArgs) {
        if (Sorted) return;
        StopDecay();
    }

    /// <summary>
    /// Function with the logic of what to do when the object is released
    /// </summary>
    /// <param name="sender">Which object grabbed this one</param>
    /// <param name="interactableObjectEventArgs">Arguments from the interactable object event</param>
    private void OnInteractableObjectUngrabbed(object sender, InteractableObjectEventArgs interactableObjectEventArgs) {
        if (Sorted) return;
        StartDecay();
    }

    /// <summary>
    /// Unity function
    /// Called when the object id destroyed
    /// </summary>
    private void OnDestroy() {
        // Stop the decay process
        CancelInvoke("Decay");
    }

    /// <summary>
    /// Function to start the decay process
    /// </summary>
    private void StartDecay() {
        // Start the decay function in 1sec and repeat it every 1sec
        InvokeRepeating("Decay", 1, 1);
    }

    /// <summary>
    /// Function to stop the decay process
    /// </summary>
    public void StopDecay() {
        // Stop the decay process
        CancelInvoke("Decay");
    }

    /// <summary>
    /// Function of decay
    /// Reduces the time, number of points, scale of the object
    /// </summary>
    private void Decay() {
        // Reduce the timer by 1
        _timer--;
        // Reduce the number of point by the reduction callculated before
        Score -= _scoreReduction;
        // Reduce the scale by the reduction callculated before
        transform.localScale = transform.localScale - _scaleReduction;
        // Check if the timer is 0
        if (_timer > 0) return; // Stop execution
        // Destroy the object
        Destroy(gameObject);
    }
}