﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <inheritdoc />
/// <summary>
/// Code that manages the game
/// </summary>
public class GameManager : MonoBehaviour {
    // Status the game can be in
    public enum Status {
        Stopped,
        Paused,
        Playing,
        Done
    }

    public Status GameStatus = Status.Stopped;  // Status the game is currently in, set to STOPPED
    public static GameManager Instance;         // Instance of the object (Singlton)
    public int StartTime;                       // The initial amount of time to play
    private float _oldScore;                    // The previous/old score

    public Text ScoreText;                      // Object where the score will be displayed
    public Text TimeText;                       // object where the time left will be displayed

    private int _timeRemaining;                 // The time remaining before the end of the game

    public int Score { get; set; }              // The total score

    /// <summary>
    /// Unity function
    /// Called before the Start() function
    /// Called once when the object is instantiated
    /// </summary>
    private void Awake() {
        // Check if the instance is null
        if (Instance == null) {
            // Set the instance to the current object
            Instance = this;
            // Call the to initialize the game
            Setup();
        } else if (Instance != null) {  // Check there is already an instance
            // Desytroy the attempt at creating a new instance
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Function to initialize the game
    /// </summary>
    private void Setup() {
        // Set the score to 0
        Score = 0;
        // Set the remaining time to the initial play time
        _timeRemaining = StartTime;
        // Display the timer
        TimeText.text = "Time : " + _timeRemaining;
        // Start the timer function in 1sec and repeat it every 1sec
        InvokeRepeating("Timer", 1, 1);
        // Call the function to update the score
        UpdateScore();
        // Change the current status to PLAYING
        GameStatus = Status.Playing;
    }

    /// <summary>
    /// Unity function
    /// Called every frame
    /// </summary>
    private void Update () {
        // Call the function to update the score
        UpdateScore();
    }

    /// <summary>
    /// Function to update the score and it's display
    /// </summary>
    private void UpdateScore() {
        // Check if there has been points added since the last update
        if (Math.Abs(_oldScore - Score) <= 0) return;   // Stop execution if no update
        // Set the old score to the new score
        _oldScore = Score;
        // Display the new score
        ScoreText.text = "Score : " + Score;
    }

    /// <summary>
    /// Function to update the timer and it's display
    /// </summary>
    private void Timer() {
        // Reduce the timer by 1
        _timeRemaining--;
        // Update the timer display
        TimeText.text = "Time : " + _timeRemaining;
        // Check if there is still time remaining
        if (_timeRemaining > 0) return;     // Stop the execution, if still time remaining
        // Set the status to DONE
        GameStatus = Status.Done;
        // Stop the timer
        CancelInvoke("Timer");
    }

    /// <summary>
    /// Function to restart the game
    /// </summary>
    public void Restart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
